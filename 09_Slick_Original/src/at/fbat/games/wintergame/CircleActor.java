package at.fbat.games.wintergame;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

public class CircleActor {
	
	private double circX, circY;
			
	
	public CircleActor(double circX, double circY) {
		super();
		this.circX = circX;
		this.circY = circY;
	}

	public void update (GameContainer gc, int delta){
		if(this.circY <= 600){
			this.circY++;
		}
		else if(this.circY >= 600){
			this.circY = 250;
		}
	}
	
	public void render (Graphics graphics){
		graphics.drawOval((float)this.circX, (float)this.circY, 100, 100);
	}

}
