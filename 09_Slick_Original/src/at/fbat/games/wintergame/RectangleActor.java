package at.fbat.games.wintergame;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

public class RectangleActor {
	private double rectX, rectY;
	private int rectDirection = 0; // 0 = right, 1 = down, 2 = left, 3 = up
		
	
	public RectangleActor(double rectX, double rectY, int rectDirection) {
		super();
		this.rectX = rectX;
		this.rectY = rectY;
		this.rectDirection = rectDirection;
	}

	public void update (GameContainer gc, int delta){
		if(this.rectDirection == 0){
			this.rectX++;
			if(this.rectX >= 650){
				this.rectDirection = 1;
			}
		}
		else if(this.rectDirection == 1){
			this.rectY++;
			if(this.rectY >= 450){
				this.rectDirection = 2;
			}
		}
		
		else if(this.rectDirection == 2){
			this.rectX--;
			if(this.rectX <= 50){
				this.rectDirection = 3;
			}
		}
		
		else if(this.rectDirection == 3){
			this.rectY--;
			if(this.rectY <= 50){
				this.rectDirection = 0;
			}
		}
	}
	
	public void render (Graphics graphics){
		graphics.drawRect((float)this.rectX, (float)this.rectY, 100, 100);
	}
}
