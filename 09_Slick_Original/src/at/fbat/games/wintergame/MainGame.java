package at.fbat.games.wintergame;

import java.util.ArrayList;
import java.util.List;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.BasicGame;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.tests.AnimationTest;

public class MainGame extends BasicGame {

	private RectangleActor ra1;
	private CircleActor ca1;
	private OvalActor oa1;
	private SnowflakeActor sfa1, sfa2, sfa3;
	private List<SnowflakeActor> snowflakes;
	private ShootingStar ss1;

	public MainGame(String title) {
		super(title);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void render(GameContainer gc, Graphics graphics) throws SlickException {

		/*this.ra1.render(graphics);
		this.ca1.render(graphics);
		this.oa1.render(graphics);

		for (SnowflakeActor snowflakeActor : this.snowflakes) {
			snowflakeActor.render(graphics);
		}
		
		this.ss1.render(graphics);
*/
	}

	@Override
	public void init(GameContainer gc) throws SlickException {
		this.ra1 = new RectangleActor(50, 50, 0);
		this.ca1 = new CircleActor(350, 250);
		this.oa1 = new OvalActor(50, 500, 0);

		this.snowflakes = new ArrayList<SnowflakeActor>();

//		for (int i = 0; i < 100; i++) {
//			this.snowflakes.add(new SnowflakeActor(300, -10, 0));
//			this.snowflakes.add(new SnowflakeActor(400, -10, 1));
//			this.snowflakes.add(new SnowflakeActor(500, -10, 2));
//		}
		
		this.ss1 = new ShootingStar();
		
	}

	@Override
	public void update(GameContainer gc, int delta) throws SlickException {

		this.ra1.update(gc, delta);
		this.ca1.update(gc, delta);
		this.oa1.update(gc, delta);

		for (SnowflakeActor snowflakeActor : this.snowflakes) {
			snowflakeActor.update(gc, delta);
		}
		
		this.ss1.update(gc, delta);
	}

	public static void main(String[] argv) {
		try {
			AppGameContainer container = new AppGameContainer(new MainGame("Wintergame"));
			container.setDisplayMode(800, 600, false);
			container.start();
		} catch (SlickException e) {
			e.printStackTrace();
		}
	}

}
