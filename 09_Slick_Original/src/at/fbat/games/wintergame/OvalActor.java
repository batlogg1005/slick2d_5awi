package at.fbat.games.wintergame;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

public class OvalActor {
	private double ovalX, ovalY;
	private int ovalDirection = 0; // 0 = right, 1 = left

	public OvalActor(double ovalX, double ovalY, int ovalDirection) {
		super();
		this.ovalX = ovalX;
		this.ovalY = ovalY;
		this.ovalDirection = ovalDirection;
	}

	public void update (GameContainer gc, int delta){
		if (this.ovalDirection == 0){
			this.ovalX++;
			if(this.ovalX >= 650){
				this.ovalDirection = 1;
			}
		}
	
		if (this.ovalDirection == 1){
			this.ovalX--;
			if(this.ovalX <= 50){
				this.ovalDirection = 0;
			}
		}
	}
	
	public void render (Graphics graphics){
		graphics.drawOval((float)this.ovalX, (float)this.ovalY, 100, 50);
	}
}
