package at.fbat.games.wintergame;

import java.util.Random;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

public class ShootingStar {

	private float ssX, ssY;
	private float ssSize = 100;
	private int ssStart;
	private int ssDuration;
	private boolean isActive = true;
	private int timeSinceStart = 0;

	/*
	 * public ShootingStar(float ssX, float ssY, float ssSize, int ssStart, int
	 * ssDuration) { super(); this.ssX = ssX; this.ssY = ssY; this.ssSize =
	 * ssSize; this.ssStart = ssStart; this.ssDuration = ssDuration; }
	 */

	private void setRandomPosition() {
		Random r = new Random();
		this.ssX = 100 + r.nextInt(600);
		this.ssY = 100 + r.nextInt(400);
		this.ssSize = 25 + r.nextInt(125);
	}

	private void setRandomTime() {
		Random r = new Random();
		this.ssStart = r.nextInt(10000);
		this.ssDuration = 2000 + r.nextInt(3000);
	}

	public void update(GameContainer gc, int delta) {

		this.timeSinceStart += delta;
		if (this.timeSinceStart >= ssStart) {
			isActive = true;
			this.ssSize = this.ssSize - (this.ssSize/this.ssDuration*delta);
		}

		if (this.timeSinceStart >= this.ssStart + this.ssDuration) {
			isActive = false;
			this.timeSinceStart = 0;
			setRandomTime();
			setRandomPosition();
		}

		System.out.println(this.toString());
	}

	public void render(Graphics graphics) {

		if (isActive == true) {
			graphics.fillOval(this.ssX, this.ssY, this.ssSize, this.ssSize);
		}
	}

	@Override
	public String toString() {
		return "ShootingStar [ssStart=" + ssStart + ", ssDuration=" + ssDuration + ", isActive=" + isActive
				+ ", timeSinceStart=" + timeSinceStart + "]";
	}

}
