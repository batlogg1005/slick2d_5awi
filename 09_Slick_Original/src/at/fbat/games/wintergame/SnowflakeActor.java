package at.fbat.games.wintergame;

import java.util.Random;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

public class SnowflakeActor {

	private float sfX, sfY, size, speed;
	private int sfType;

	// enum
	public SnowflakeActor(float sfX, float sfY, int sfType) {
		super();

		this.sfX = sfX;
		this.sfY = sfY;

		setRandomPostion();

		this.sfType = sfType;

		if (this.sfType == 0) {
			this.size = 10;
			this.speed = 0.2f;
		}

		else if (this.sfType == 1) {
			this.size = 20;
			this.speed = 0.5f;
		}

		else if (this.sfType == 2) {
			this.size = 30;
			this.speed = 1.0f;
		}

	}

	private void setRandomPostion() {
		Random r = new Random();
		this.sfX = r.nextInt(800);
		this.sfY = -r.nextInt(600);
	}

	public void update(GameContainer gc, int delta) {

		this.sfY = this.sfY + (delta * this.speed);

		if (this.sfY >= 600) {
			setRandomPostion();
		}
	}

	public void render(Graphics graphics) {

		graphics.fillOval(this.sfX, this.sfY, this.size, this.size);
	}

}
