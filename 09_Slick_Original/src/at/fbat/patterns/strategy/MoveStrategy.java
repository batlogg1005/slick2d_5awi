package at.fbat.patterns.strategy;

public interface MoveStrategy {
	public float getX();
	public float getY();
	public void update (int delta);
	public void move(int delta);
}
