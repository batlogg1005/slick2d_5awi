package at.fbat.patterns.observer;

public interface Observer {
	public void inform();
}
