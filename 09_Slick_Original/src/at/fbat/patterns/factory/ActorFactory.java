package at.fbat.patterns.factory;

import java.util.ArrayList;
import java.util.Random;

import at.fbat.patterns.actor.Actor;
import at.fbat.patterns.actor.HTLOval;
import at.fbat.patterns.strategy.MoveLeft;
import at.fbat.patterns.strategy.MoveRight;
import at.fbat.patterns.strategy.MoveStrategy;

public class ActorFactory {

	public static ArrayList<Actor> generateActors(int count){
		
		Random rnd = new Random();
		ArrayList<Actor> Actors = new ArrayList<>();
		
		for(int i = 0; i < count; i++){
			
			MoveStrategy ms = rnd.nextInt(100) % 2 == 0 ? new MoveRight(0, 0, 1) : new MoveLeft(0, 0, 1);
			//? kurzform f�r if() else()
			
			Actors.add(new HTLOval(ms));
			
		}
		
		
		return Actors;
		
	}
	
}
