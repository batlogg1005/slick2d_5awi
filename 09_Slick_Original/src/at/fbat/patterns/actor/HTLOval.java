package at.fbat.patterns.actor;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

import at.fbat.patterns.observer.Observer;
import at.fbat.patterns.strategy.MoveStrategy;

public class HTLOval implements Actor, Observer {

	private MoveStrategy moveStrategy;
	private Color color;

	public HTLOval(MoveStrategy strategy) {
		this.moveStrategy = strategy;
	}

	@Override
	public void draw(Graphics graphics) {
		graphics.setColor(this.color);
		graphics.fillOval(this.moveStrategy.getX(), this.moveStrategy.getY(), 20, 20);
		graphics.setColor(Color.white);
	}

	@Override
	public void move(GameContainer gc, int delta) {
		moveStrategy.move(delta);
	}

	public void inform() {
		this.color = Color.orange;
	}

}
