package at.fbat.patterns.actor;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

public interface Actor {
	public void draw(Graphics graphics);
	public void move(GameContainer gc, int delta);
}
