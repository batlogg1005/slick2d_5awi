package at.fbat.patterns.actor;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

import at.fbat.patterns.observer.Observer;
import at.fbat.patterns.strategy.MoveStrategy;

public class HTLRectangle extends AbstractActor implements Observer {

	private Color color;

	public HTLRectangle(MoveStrategy mr) {
		super(mr);
		this.color = Color.white;

	}

	@Override
	public void draw(Graphics graphics) {
		graphics.setColor(this.color);
		graphics.drawRect(this.moveStrategy.getX(), this.moveStrategy.getY(), 20, 20);
		graphics.setColor(Color.white);
	}

	@Override
	public void inform() {
		this.color = Color.pink;

	}

}
