package at.fbat.patterns.actor;

import java.util.ArrayList;
import java.util.Random;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

import at.fbat.patterns.strategy.*;

public abstract class AbstractActor implements Actor {

	protected MoveStrategy moveStrategy;

	public AbstractActor(MoveStrategy moveStrategy) {
		super();
		this.moveStrategy = moveStrategy;
	}

	@Override
	public void move(GameContainer gc, int delta) {
		moveStrategy.update(delta);
	}
	


}
