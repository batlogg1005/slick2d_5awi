package at.fbat.patterns.basics;

import java.util.ArrayList;
import java.util.List;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.BasicGame;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;

import at.fbat.games.wintergame.MainGame;
import at.fbat.patterns.actor.Actor;
import at.fbat.patterns.actor.HTLCircle;
import at.fbat.patterns.actor.HTLOval;
import at.fbat.patterns.actor.HTLRectangle;
import at.fbat.patterns.actor.Player;
import at.fbat.patterns.factory.ActorFactory;
import at.fbat.patterns.strategy.MoveLeft;
import at.fbat.patterns.strategy.MoveRight;
import at.fbat.patterns.strategy.MoveStrategy;

public class Main_Pattern extends BasicGame {

	private List<Actor> actors;
	private Player player;

	public Main_Pattern(String title) {
		super(title);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void render(GameContainer gc, Graphics graphics) throws SlickException {
		// TODO Auto-generated method stub
		for (Actor actor : this.actors) {
			actor.draw(graphics);
		}
	}

	@Override
	public void init(GameContainer arg0) throws SlickException {
		// TODO Auto-generated method stub
		this.actors = new ArrayList<>();
		MoveStrategy mr1 = new MoveRight(0, 0, 0.3f);
		MoveStrategy ml1 = new MoveLeft(500, 100, 0.1f);
		MoveStrategy ml2 = new MoveLeft(540, 200, 0.2f);
		
		ActorFactory af1 = new ActorFactory();
		this.actors.add(af1.generateActors(10));

		this.player = new Player();

		HTLCircle c1 = new HTLCircle(mr1);
		this.actors.add(c1);
		HTLCircle c2 = new HTLCircle(ml1);
		this.actors.add(c2);

		HTLRectangle r1 = new HTLRectangle(ml2);
		this.actors.add(r1);
		this.player.addObserver(r1);

		HTLOval o0 = new HTLOval(ml2);
		this.actors.add(o0);
		this.player.addObserver(o0);

		HTLOval o1 = new HTLOval(new MoveRight(10, 200, 0.15f));
		this.actors.add(o1);
		this.player.addObserver(o1);
		
		actors.add(player);
		
		

	}

	@Override
	public void update(GameContainer gc, int delta) throws SlickException {
		// TODO Auto-generated method stub
		for (Actor actor : this.actors) {
			actor.move(gc, delta);
		}
	}

	public static void main(String[] argv) {
		try {
			AppGameContainer container = new AppGameContainer(new Main_Pattern("Main_Pattern"));
			container.setDisplayMode(800, 600, false);
			container.start();
		} catch (SlickException e) {
			e.printStackTrace();
		}
	}

}
