package at.gad.Universe;

import java.util.Random;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;

public class Star {
	Random random = new Random();
	private float x;
	private float y;
	private float z;
	private float sy;
	private float sx;
	private int height;
	private int width;
	private float r;
	private float speed;
	private float pz = z;
	private float px;
	private float py;
	private Image snowflake;
	
	
	public Star(float x, float y, float z, float sx, float sy){
		this.x = x;
		this.y = y;
		this.z = z;
		this.sx = sx;
		this.sy = sy;
		this.pz = this.z;
			
		}
	
	public void render (Graphics g) throws SlickException{
		this.sx = (this.x / this.z) * 1000;
		this.sy = (this.y / this.z)* 1000;
		this.r = 600/z;
		System.out.println("Z: "+this.z);
		//this.snowflake = new Image("testdata/snowflake.png");
		//g.drawImage(snowflake, sx,sy);
		g.fillOval(sx, sy, this.r, this.r);
		
		this.px =(this.x / this.pz) * 1000;
		this.py =(this.y / this.pz) * 1000;
		g.drawLine(px, py, sx, sy);
		
		this.pz = this.z;
		
	}

	public void update (GameContainer gc){
		this.pz = this.z;
		this.height = gc.getHeight();
		this.width = gc.getWidth();
		speed = gc.getInput().getMouseX() /25  +1;
		this.z = this.z -speed;
		if (this.z < 1) {
			this.z = random.nextInt(width);
			this.x = random.nextInt(width)-width/2;
			this.y = random.nextInt(height)-height/2;
			this.pz = this.z;
		}
	}
}